# WebLab Portlet Filter

This project is a Maven based Java project of a Taglib.

This library aims to provide a central access point to the portlet event mapping and redirection
Each portlet to integrate in a weblab application must to register action et event filter in the their "portlet.xml" file

The mapping is done using a remote exposed bean at the URL define in the system variable "weblab.portlet.fitler.url"

If the system variable is not found, the mapping is done in a simple spring config file "portlet-filter-mapping.xml" which must be accessible in JAVA of the portlet.

# Build status
## Master
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-portlet-filters/badges/master/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-portlet-filters/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-portlet-filters/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-portlet-filters/commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
