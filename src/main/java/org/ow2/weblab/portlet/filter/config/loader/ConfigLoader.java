/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.filter.config.loader;

import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Map;

import javax.management.InstanceAlreadyExistsException;
import javax.management.JMX;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.filter.config.PortletMappingBean;

/**
 * Try several configuration loader in the following order:
 * <ol>
 * <li>Connect to the bus registry to gather information on deployed services</li>
 * <li>Connect to the weblab.portlet.filter.queue JMX queue to ask/listen for broadcasted action/reaction/redirection map configuration</li>
 * <li>Connect to a HTTP URL defined in system property weblab.portlet.filter.url to retrieve action/reaction/redirection map configuration</li>
 * <li>Use local bean in classpath</li>
 * </ol>
 *
 * @author asaval, ebondu
 */
public abstract class ConfigLoader implements ConfigLoaderMBean {


	public static final String ENV_WEBLAB_URL = "weblab.portlet.filter.url";


	protected static final Log LOGGER = LogFactory.getLog(ConfigLoader.class);


	protected final Log log = LogFactory.getLog(this.getClass());


	protected PortletMappingBean pmb;


	protected final String portletName;


	/**
	 * @param portletName
	 *            the portlet name
	 */
	public ConfigLoader(final String portletName) {
		this.portletName = portletName;
		this.pmb = new PortletMappingBean();
	}



	public static PortletMappingBean getInstance(final String portletName) {
		ConfigLoader.LOGGER.info("Loading filters for portlet: " + portletName);

		// check if MBean for this portlet is registered
		final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = null;
		try {
			name = new ObjectName("WebLab:type=PortletFiltersMBean,name=" + portletName);
			if (mbs.isRegistered(name)) {
				ConfigLoader.LOGGER.debug("WebLab portlet filter " + portletName + " already stored as MBean. Try to reuse the same instance.");
				// we return this instance
				final ConfigLoaderMBean clmb = JMX.newMBeanProxy(mbs, name, ConfigLoaderMBean.class, true);
				if (portletName.equalsIgnoreCase(clmb.getName())) {
					ConfigLoader.LOGGER.info("Reuse the same instance of WebLab portlet filter for " + portletName + ".");
					return new PortletMappingBean(portletName, clmb.getInstance());
				}
				ConfigLoader.LOGGER.error("Filter configuration missmatch between " + portletName + " " + clmb.getName() + ".");
			}// else we create init the config
		} catch (final MalformedObjectNameException e1) {
			ConfigLoader.LOGGER.error(e1.getMessage(), e1);
		} catch (final NullPointerException e1) {
			ConfigLoader.LOGGER.error(e1.getMessage(), e1);
		}


		ConfigLoader confLoader;
		try {
			confLoader = new SpringBeanConfigLoader(portletName);
		} catch (final Exception e) {
			ConfigLoader.LOGGER.error(e.getMessage(), e);
			return null;
		}

		ConfigLoader.LOGGER.info("WebLab portlet filter uses " + confLoader.getClass().getName() + " : " + confLoader.getInfos());
		// we register the config
		if (name != null) {
			try {
				mbs.registerMBean(confLoader, name);
				ConfigLoader.LOGGER.debug("WebLab portlet-filters for " + portletName + " is now registered in MBeanServer registry.");
			} catch (final InstanceAlreadyExistsException e) {
				ConfigLoader.LOGGER.error(e.getMessage(), e);
			} catch (final MBeanRegistrationException e) {
				ConfigLoader.LOGGER.error(e.getMessage(), e);
			} catch (final NotCompliantMBeanException e) {
				ConfigLoader.LOGGER.error(e.getMessage(), e);
			}
		} else {
			ConfigLoader.LOGGER.warn("Could not create ObjectName filter configuration for portlet " + portletName);
			ConfigLoader.LOGGER.warn("Dynamic portlet filters may not work.");
		}

		return confLoader.pmb;
	}


	@Override
	public Map<?, ?>[] getInstance() {
		return new Map[] { this.loadActionConfig(), this.loadReactionConfig(), this.loadRedirection() };
	}


	public Map<String, String> loadActionConfig() {
		if (this.pmb.getActions().isEmpty()) {
			this.pmb.setActions(this.loadAction());
		}
		return this.pmb.getActions();
	}


	public Map<String, List<String>> loadReactionConfig() {
		if (this.pmb.getReactions().isEmpty()) {
			this.pmb.setReactions(this.loadReaction());
		}
		return this.pmb.getReactions();
	}


	public Map<String, String> loadRedirectionConfig() {
		if (this.pmb.getRedirections().isEmpty()) {
			this.pmb.setRedirections(this.loadRedirection());
		}
		return this.pmb.getRedirections();
	}


	protected abstract Map<String, String> loadAction();


	protected abstract Map<String, List<String>> loadReaction();


	protected abstract Map<String, String> loadRedirection();


	@Override
	public abstract boolean isAvailable();


	@Override
	public abstract String getInfos();

}
