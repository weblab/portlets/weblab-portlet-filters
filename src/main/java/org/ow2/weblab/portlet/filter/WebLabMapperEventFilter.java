/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.EventFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.filter.config.PortletMappingBean;
import org.ow2.weblab.portlet.filter.config.loader.ConfigLoader;

/**
 * Portlet events filter.
 * Incoming and outcoming events qname are mapped to portlets actions/reactions qname.
 *
 * @author emilienbondu
 *
 */
public class WebLabMapperEventFilter implements EventFilter {


	private String portletName;


	private Map<String, List<String>> reaction;


	private Map<String, String> action;


	private final Log logger = LogFactory.getLog(this.getClass());


	@Override
	public void doFilter(final EventRequest request, final EventResponse response, final FilterChain chain) throws IOException, PortletException {
		this.logger.debug("WebLab event filter called in " + this.portletName + " for event " + request.getEvent().getQName().toString() + ".");

		// Fitlering incoming event
		QName reactionQN = null;
		for (final Entry<String, List<String>> entry : this.reaction.entrySet()) {
			for (final String qn : entry.getValue()) {
				if (qn.toString().equalsIgnoreCase(request.getEvent().getQName().toString())) {
					reactionQN = QName.valueOf(entry.getKey());
					break;
				}
			}
		}
		// wrapping the original response
		if (reactionQN != null) {
			this.logger.debug("Mapping found for this event wrapping it.");
			final EventRequest newRequest = new EventRequestImpl(request, reactionQN);
			final EventResponseImpl newResponse = new EventResponseImpl(response, this.action);
			chain.doFilter(newRequest, newResponse);
			this.logger.debug("Mapping found for incoming event, modifying the event Qname : " + request.getEvent().getQName() + " -> " + reactionQN);
		} else {
			this.logger.warn("No mapping found for the event incoming event with QName :" + request.getEvent().getQName() + ". STOPPING EVENT PROPAGATION");
		}
	}


	@Override
	public void destroy() {
		this.reaction = null;
		this.action = null;
		this.portletName = null;
	}


	@Override
	public void init(final FilterConfig filterConfig) throws PortletException {
		this.logger.debug("Very start of WebLab Event filter initialisation.");
		this.portletName = filterConfig.getPortletContext().getPortletContextName();
		this.logger.info("Initialisation of WebLab Event filter for " + this.portletName + ".");

		// Load filter configuration using ConfigLoader classloader
		final ClassLoader current_classloader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(ConfigLoader.class.getClassLoader());
		final PortletMappingBean instance = ConfigLoader.getInstance(this.portletName);
		Thread.currentThread().setContextClassLoader(current_classloader);

		if (instance == null) {
			final String message = "Cannot instanciate ConfigLoader from WebLab Event filter for " + this.portletName + ".";
			this.logger.error(message);
			throw new PortletException(message);
		}

		this.reaction = instance.getReactions();
		this.action = instance.getActions();
		this.logger.debug(this.reaction);
	}

}
