/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter;

import java.io.IOException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.filter.config.PortletMappingBean;
import org.ow2.weblab.portlet.filter.config.loader.ConfigLoader;

/**
 * Portlet actions filter.
 * Outcoming event qname are mapped to portlets actions qname.
 * Trying to map to redirect URL when send an event.
 *
 * @author emilienbondu
 *
 */
public class WebLabMapperActionFilter implements ActionFilter {


	private String portletName;


	private Map<String, String> redirection;


	private Map<String, String> action;


	private final Log logger = LogFactory.getLog(this.getClass());


	@Override
	public void destroy() {
		this.redirection = null;
		this.action = null;
		this.portletName = null;
	}


	@Override
	public void init(final FilterConfig filterConfig) throws PortletException {
		this.logger.debug("Very start of WebLab Action filter initialisation.");
		this.portletName = filterConfig.getPortletContext().getPortletContextName();
		this.logger.info("Initialisation of WebLab Action filter for " + this.portletName + ".");

		// Load filter configuration using ConfigLoader classloader
		final ClassLoader current_classloader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(ConfigLoader.class.getClassLoader());
		final PortletMappingBean instance = ConfigLoader.getInstance(this.portletName);
		Thread.currentThread().setContextClassLoader(current_classloader);

		if (instance == null) {
			final String message = "Cannot instanciate ConfigLoader from WebLab Action filter for " + this.portletName + ".";
			this.logger.error(message);
			throw new PortletException(message);
		}

		this.redirection = instance.getRedirections();
		this.action = instance.getActions();
	}


	@Override
	public void doFilter(final ActionRequest request, final ActionResponse response, final FilterChain chain) throws IOException, PortletException {
		this.logger.debug("WebLab action filter called in " + this.portletName + ".");

		// wrapping the original response
		final ActionResponseImpl newResponse = new ActionResponseImpl(response, this.action, this.redirection);
		chain.doFilter(request, newResponse);
	}

}
