/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter.config;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

/**
 * A mapping bean for portlet action / reaction redirection
 *
 * @author emilienbondu
 */
public class PortletMappingBean {


	private Map<String, String> actions;


	private Map<String, List<String>> reactions;


	private Map<String, String> redirections;


	private final Log logger = LogFactory.getLog(this.getClass());


	/**
	 * Default constructor initialising empty maps
	 */
	public PortletMappingBean() {
		this.actions = new HashMap<>();
		this.reactions = new HashMap<>();
		this.redirections = new HashMap<>();
	}


	/**
	 * Creates a PortletMappingBean from a ConfigLoader array of maps
	 * 
	 * @param portletName
	 *            The name of the portlet configuration
	 * @param maps
	 *            The maps of action, reaction and redirection
	 */

	public PortletMappingBean(final String portletName, final Map<?, ?>[] maps) {
		if (maps == null) {
			throw new IllegalArgumentException("JMX array of maps from ConfigLoader can not be null when initializing PortletMappingBean.");
		}
		if (maps.length < 3) {
			throw new IllegalArgumentException(
					"PortletMappingBean can only be configured with JMX array of maps from ConfigLoader, the arry must have the three maps (actions, reactions and redirection), given array was : "
							+ Arrays.asList(maps));
		}
		this.setActions(maps[0]);
		this.setReactions(maps[1]);
		this.setRedirections(maps[2]);
	}


	public Map<String, String> getActions() {
		return this.actions;
	}


	public void setActions(final Map<?, ?> actions) {
		this.actions = new HashMap<>();
		if (actions != null) {
			for (final Entry<?, ?> entry : actions.entrySet()) {
				final Object objectKey = entry.getKey();
				if (!(objectKey instanceof String)) {
					this.logger.warn("Invalid action definition: key is not a String but a '" + objectKey.getClass() + "'.");
					continue;
				}
				final Object objectValue = entry.getValue();
				if (!(objectValue instanceof String)) {
					this.logger.warn("Invalid action definition: value is not a String but a '" + objectValue.getClass() + "' (Key was '" + objectKey + "'.");
					continue;
				}
				try {
					final String key = PortletMappingBean.validateQName(((String) objectKey).trim());
					final String value = PortletMappingBean.validateQName(((String) objectValue).trim());
					this.actions.put(key, value);
				} catch (final IllegalArgumentException iae) {
					this.logger.warn("Invalid action definition: '" + objectKey + "' - '" + objectValue + "'.", iae);
				}
			}
		}
	}


	public Map<String, String> getRedirections() {
		return this.redirections;
	}


	public void setRedirections(final Map<?, ?> redirections) {
		this.redirections = new HashMap<>();
		if (redirections != null) {
			for (final Entry<?, ?> entry : redirections.entrySet()) {
				final Object objectKey = entry.getKey();
				if (!(objectKey instanceof String)) {
					this.logger.warn("Invalid redirection definition: key is not a String but a '" + objectKey.getClass() + "'.");
					continue;
				}
				final Object objectValue = entry.getValue();
				if (!(objectValue instanceof String)) {
					this.logger.warn("Invalid action redirection: value is not a String but a '" + objectValue.getClass() + "' (Key was '" + objectKey + "'.");
					continue;
				}
				try {
					this.redirections.put(PortletMappingBean.validateQName(((String) objectKey).trim()), ((String) objectValue).trim());
				} catch (final IllegalArgumentException iae) {
					this.logger.warn("Invalid redirection key: '" + objectKey + "'.", iae);
				}
			}
		}
	}


	public Map<String, List<String>> getReactions() {
		return this.reactions;
	}


	public void setReactions(final Map<?, ?> reactions) {
		this.reactions = new HashMap<>();
		if (reactions != null) {
			for (final Entry<?, ?> entry : reactions.entrySet()) {
				final Object objectKey = entry.getKey();
				if (!(objectKey instanceof String)) {
					this.logger.warn("Invalid reaction definition: key is not a String but a '" + objectKey.getClass() + "'.");
					continue;
				}
				final String key;
				try {
					key = PortletMappingBean.validateQName(((String) objectKey).trim());
				} catch (final IllegalArgumentException iae) {
					this.logger.warn("Invalid reaction key: '" + objectKey + "'.", iae);
					continue;
				}
				final Object objectValue = entry.getValue();
				if (!(objectValue instanceof List)) {
					this.logger.warn("Invalid action redirection: value is not a List but a '" + objectValue.getClass() + "' (Key was '" + objectKey + "'.");
					continue;
				}

				final List<String> values = new LinkedList<>();
				this.reactions.put(key, values);
				for (final Object originalValue : (List<?>) entry.getValue()) {
					if (!(originalValue instanceof String)) {
						this.logger.warn("Invalid redirection definition: value is not a String but a '" + originalValue.getClass() + "'.");
						continue;
					}
					final String value;
					try {
						value = PortletMappingBean.validateQName(((String) originalValue).trim());
						values.add(value);
					} catch (final IllegalArgumentException iae) {
						this.logger.warn("Invalid reaction value: '" + originalValue + "'.", iae);
					}
				}
			}
		}
	}


	@Override
	public String toString() {
		return "Actions:" + this.actions + "\nReactions:" + this.reactions + "\nRedirection:" + this.redirections;
	}


	private static String validateQName(final String qnameStr) {
		final QName qname = QName.valueOf(qnameStr);

		if ((URI.create(qname.getNamespaceURI()) != null) && !StringUtils.containsWhitespace(qname.getLocalPart())) {
			return qname.toString();
		}
		throw new IllegalArgumentException("qname localpart contains invalid spaces.");
	}


}
