/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.filter.config.loader;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.ow2.weblab.portlet.filter.config.PortletMappingBean;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.UrlResource;

/**
 * Implementation of configuration loader based on Spring.
 *
 * @author WebLab Team
 */
public class SpringBeanConfigLoader extends ConfigLoader {


	private static final String DEFAULT_CONFIGURATION_FILE = "portlet-filter-mapping.xml";


	private AbstractXmlApplicationContext factory;


	/**
	 * @param portletName
	 *            The name of the portlet. Used to get the name of the bean describing the action/reaction/redirection
	 * @throws Exception
	 *             If the configuration file cannot be loaded
	 */
	public SpringBeanConfigLoader(final String portletName) throws Exception {
		super(portletName);
		this.factory = SpringBeanConfigLoader.loadFactory();
		if (this.factory == null) {
			throw new Exception("Cannot instanciate WebLab portlet filter neither from environement variable " + ENV_WEBLAB_URL + " nor from classpath file " + DEFAULT_CONFIGURATION_FILE + ".");
		}
		this.factory.refresh();
		if (this.factory.containsBean(this.portletName)) {
			this.pmb = this.factory.getBean(this.portletName, PortletMappingBean.class);
		} else {
			this.log.error("Unable to load filter bean for '" + this.portletName + "'. Using an empty one.");
		}
	}


	/**
	 * @return The loaded bean factory
	 */
	private static AbstractXmlApplicationContext loadFactory() {
		String envProperty = System.getProperty(ENV_WEBLAB_URL);
		if (envProperty == null) {
			LOGGER.warn("Property " + ENV_WEBLAB_URL + " is not defined. Try to load filters from classpath.");
		} else {
			try {
				URL url = new URL(envProperty);
				if (new UrlResource(url).exists()) {
					return new UrlXmlApplicationContext(url);
				}
				LOGGER.warn("No XML Spring Application Context file found at " + url + ". Try to load filters from classpath.");
			} catch (final MalformedURLException murle) {
				LOGGER.warn("Property " + ENV_WEBLAB_URL + " contains an invalid URL value (" + envProperty + ". Try to load filters from classpath.", murle);
			}
		}
		if (new ClassPathResource(SpringBeanConfigLoader.DEFAULT_CONFIGURATION_FILE).exists()) {
			return new ClassPathXmlApplicationContext(SpringBeanConfigLoader.DEFAULT_CONFIGURATION_FILE);
		}
		LOGGER.warn("File " + SpringBeanConfigLoader.DEFAULT_CONFIGURATION_FILE + " not found inside classpath");
		return null;
	}


	@Override
	public boolean isAvailable() {
		return this.factory.containsBean(this.portletName);
	}


	@Override
	public String getInfos() {
		return "Mapping for the portlet " + this.portletName + " from factory: " + this.factory + ". ACTIONS: " + this.loadActionConfig() + ", REACTIONS: " + this.loadReactionConfig()
				+ "REDIRECTIONS: " + this.loadRedirectionConfig();
	}


	@Override
	protected Map<String, String> loadAction() {
		return this.pmb.getActions();
	}


	@Override
	protected Map<String, List<String>> loadReaction() {
		return this.pmb.getReactions();
	}


	@Override
	protected Map<String, String> loadRedirection() {
		return this.pmb.getRedirections();
	}


	@Override
	public String getName() {
		return this.portletName;
	}

}
