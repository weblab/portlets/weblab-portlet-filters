/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.filter.config.loader;

import java.util.Map;

/**
 * A simple MBean interface to access ConfigLoader from JMX.
 * 
 * @author WebLab Team
 */
public interface ConfigLoaderMBean {


	/**
	 * @return The portlet name for the config loader
	 */
	public String getName();


	/**
	 * returns true if this configloader is available, else false
	 *
	 * @return true if this configloader is available, else false
	 */
	public boolean isAvailable();


	/**
	 * Return infos about the config loader
	 *
	 * @return The infos about the config loader
	 */
	public String getInfos();

	/**
	 * Returns an array containing in the following order:
	 * <ol>
	 * <li>a map of actions</li>
	 * <li>a map of reactions</li>
	 * <li>a map of redirections</li>
	 * </ol>
	 *
	 * @return an array of map of actions, map of reactions, map of redirections
	 */
	public Map<?, ?>[] getInstance();

}
