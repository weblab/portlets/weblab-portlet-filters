/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.portlet.filter.ActionResponseWrapper;
import javax.servlet.http.Cookie;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

/**
 * Action response wrapper.
 * Try to map event before to send it to the original response.
 * Try to map for redirection after setting the event to the original response.
 *
 * @author emilienbondu
 */
public class ActionResponseImpl extends ActionResponseWrapper {


	private final ActionResponse originalResponse;


	private final Map<String, String> action;


	private final Map<String, String> redirection;


	private final Log logger = LogFactory.getLog(this.getClass());


	public ActionResponseImpl(final ActionResponse response, final Map<String, String> action, final Map<String, String> redirection) {
		super(response);
		this.originalResponse = response;
		this.action = action;
		this.redirection = redirection;
	}


	@Override
	public PortletMode getPortletMode() {
		return this.originalResponse.getPortletMode();
	}


	@Override
	public Map<String, String[]> getRenderParameterMap() {
		return this.originalResponse.getRenderParameterMap();
	}


	@Override
	public WindowState getWindowState() {
		return this.originalResponse.getWindowState();
	}


	@Override
	public void removePublicRenderParameter(final String name) {
		this.originalResponse.removePublicRenderParameter(name);
	}


	@Override
	public void setEvent(final QName name, final Serializable value) {
		// looking for event qname mapping
		for (final Entry<String, String> entry : this.action.entrySet()) {
			if (name.toString().equalsIgnoreCase(entry.getKey())) {
				final QName actionQN = QName.valueOf(entry.getValue());
				this.logger.debug("Mapping found for outcoming event, modifying the event Qname : " + name + " -> " + actionQN);
				this.originalResponse.setEvent(actionQN, value);
				break;
			}
		}

		// looking for redirection
		for (final Entry<String, String> entry : this.redirection.entrySet()) {
			if (name.toString().equalsIgnoreCase(entry.getKey())) {
				final String location = entry.getValue();
				this.logger.debug("Mapping found for outcoming event, making redirection to : " + location);
				try {
					this.originalResponse.sendRedirect(location);
				} catch (final IOException e) {
					this.logger.error(e);
				}
				break;
			}
		}
	}


	@Override
	public void setEvent(final String name, final Serializable value) {
		this.originalResponse.setEvent(name, value);
	}


	@Override
	public void setPortletMode(final PortletMode portletMode) throws PortletModeException {
		this.originalResponse.setPortletMode(portletMode);
	}


	@Override
	public void setRenderParameter(final String key, final String value) {
		this.originalResponse.setRenderParameter(key, value);
	}


	@Override
	public void setRenderParameter(final String key, final String[] values) {
		this.originalResponse.setRenderParameter(key, values);
	}


	@Override
	public void setRenderParameters(final Map<String, String[]> parameters) {
		this.originalResponse.setRenderParameters(parameters);
	}


	@Override
	public void setWindowState(final WindowState windowState) throws WindowStateException {
		this.originalResponse.setWindowState(windowState);
	}


	@Override
	public void addProperty(final Cookie cookie) {
		this.originalResponse.addProperty(cookie);
	}


	@Override
	public void addProperty(final String key, final String value) {
		this.originalResponse.addProperty(key, value);
	}


	@Override
	public void addProperty(final String key, final Element element) {
		this.originalResponse.addProperty(key, element);
	}


	@Override
	public Element createElement(final String tagName) throws DOMException {
		return this.originalResponse.createElement(tagName);
	}


	@Override
	public String encodeURL(final String path) {
		return this.originalResponse.encodeURL(path);
	}


	@Override
	public String getNamespace() {
		return this.originalResponse.getNamespace();
	}


	@Override
	public void setProperty(final String key, final String value) {
		this.originalResponse.setProperty(key, value);
	}


	@Override
	public void sendRedirect(final String location) throws IOException {
		this.originalResponse.sendRedirect(location);
	}


	@Override
	public void sendRedirect(final String location, final String renderUrlParamName) throws IOException {
		this.originalResponse.sendRedirect(location, renderUrlParamName);
	}
}
