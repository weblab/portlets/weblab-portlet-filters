/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.filter.config.loader;

import java.net.URL;

import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;



/**
 * This class is an implementation of AbstractXmlApplicationContext enabling the loading of an Application Context based on a provided URL.
 *
 * @author ymombrun
 */
public class UrlXmlApplicationContext extends AbstractXmlApplicationContext {


	final URL url;


	/**
	 * @param url
	 *            The URL where is located the Xml file
	 */
	public UrlXmlApplicationContext(final URL url) {
		this.url = url;
	}


	@Override
	protected Resource[] getConfigResources() {
		return new Resource[] { new UrlResource(this.url) };
	}


}
