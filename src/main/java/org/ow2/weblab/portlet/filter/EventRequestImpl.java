/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter;

import java.io.Serializable;
import java.security.Principal;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.PortalContext;
import javax.portlet.PortletMode;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.WindowState;
import javax.portlet.filter.EventRequestWrapper;
import javax.servlet.http.Cookie;
import javax.xml.namespace.QName;


/**
 * Event request wrapper.
 * Try to map event before to send it to the original response.
 *
 * @author emilienbondu
 *
 */
public class EventRequestImpl extends EventRequestWrapper {


	final EventRequest original;


	final QName newQN;


	public EventRequestImpl(final EventRequest original, final QName newQname) {
		super(original);
		this.original = original;
		this.newQN = newQname;
	}


	@Override
	public Event getEvent() {
		return new Event() {


			@Override
			public Serializable getValue() {
				return EventRequestImpl.this.original.getEvent().getValue();
			}


			@Override
			public QName getQName() {
				return EventRequestImpl.this.newQN;
			}


			@Override
			public String getName() {
				return EventRequestImpl.this.original.getEvent().getName();
			}
		};
	}


	@Override
	public String getMethod() {
		return this.original.getMethod();
	}


	@Override
	public Object getAttribute(final String name) {
		return this.original.getAttribute(name);
	}


	@Override
	public Enumeration<String> getAttributeNames() {
		return this.getAttributeNames();
	}


	@Override
	public String getAuthType() {
		return this.original.getAuthType();
	}


	@Override
	public String getContextPath() {
		return this.original.getContextPath();
	}


	@Override
	public Cookie[] getCookies() {
		return this.original.getCookies();
	}


	@Override
	public Locale getLocale() {
		return this.original.getLocale();
	}


	@Override
	public Enumeration<Locale> getLocales() {
		return this.original.getLocales();
	}


	@Override
	public String getParameter(final String name) {
		return this.original.getParameter(name);
	}


	@Override
	public Map<String, String[]> getParameterMap() {
		return this.original.getParameterMap();
	}


	@Override
	public Enumeration<String> getParameterNames() {
		return this.original.getParameterNames();
	}


	@Override
	public String[] getParameterValues(final String name) {
		return this.original.getParameterValues(name);
	}


	@Override
	public PortalContext getPortalContext() {
		return this.original.getPortalContext();
	}


	@Override
	public PortletMode getPortletMode() {
		return this.original.getPortletMode();
	}


	@Override
	public PortletSession getPortletSession() {
		return this.original.getPortletSession();
	}


	@Override
	public PortletSession getPortletSession(final boolean create) {
		return this.original.getPortletSession(create);
	}


	@Override
	public PortletPreferences getPreferences() {
		return this.original.getPreferences();
	}


	@Override
	public Map<String, String[]> getPrivateParameterMap() {
		return this.original.getPrivateParameterMap();
	}


	@Override
	public Enumeration<String> getProperties(final String name) {
		return this.original.getProperties(name);
	}


	@Override
	public String getProperty(final String name) {
		return this.original.getProperty(name);
	}


	@Override
	public Enumeration<String> getPropertyNames() {
		return this.original.getPropertyNames();
	}


	@Override
	public Map<String, String[]> getPublicParameterMap() {
		return this.original.getPublicParameterMap();
	}


	@Override
	public String getRemoteUser() {
		return this.original.getRemoteUser();
	}


	@Override
	public String getRequestedSessionId() {
		return this.original.getRequestedSessionId();
	}


	@Override
	public String getResponseContentType() {
		return this.original.getResponseContentType();
	}


	@Override
	public Enumeration<String> getResponseContentTypes() {
		return this.original.getResponseContentTypes();
	}


	@Override
	public String getScheme() {
		return this.original.getScheme();
	}


	@Override
	public String getServerName() {
		return this.original.getServerName();
	}


	@Override
	public int getServerPort() {
		return this.original.getServerPort();
	}


	@Override
	public Principal getUserPrincipal() {
		return this.original.getUserPrincipal();
	}


	@Override
	public String getWindowID() {
		return this.original.getWindowID();
	}


	@Override
	public WindowState getWindowState() {
		return this.original.getWindowState();
	}


	@Override
	public boolean isPortletModeAllowed(final PortletMode mode) {
		return this.original.isPortletModeAllowed(mode);
	}


	@Override
	public boolean isRequestedSessionIdValid() {
		return this.original.isRequestedSessionIdValid();
	}


	@Override
	public boolean isSecure() {
		return this.original.isSecure();
	}


	@Override
	public boolean isUserInRole(final String role) {
		return this.isUserInRole(role);
	}


	@Override
	public boolean isWindowStateAllowed(final WindowState state) {
		return this.original.isWindowStateAllowed(state);
	}


	@Override
	public void removeAttribute(final String name) {
		this.original.removeAttribute(name);
	}


	@Override
	public void setAttribute(final String name, final Object o) {
		this.original.setAttribute(name, o);
	}

}
