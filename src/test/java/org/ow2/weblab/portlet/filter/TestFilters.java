/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.filter;

import java.lang.management.ManagementFactory;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.portlet.filter.config.PortletMappingBean;
import org.ow2.weblab.portlet.filter.config.loader.ConfigLoader;


public class TestFilters {


	@Before
	public void clearMBeans() throws Exception {
		final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName urlName = new ObjectName("WebLab:type=PortletFiltersMBean,name=url");
		if (mbs.isRegistered(urlName)) {
			mbs.unregisterMBean(urlName);
		}
		ObjectName classpathName = new ObjectName("WebLab:type=PortletFiltersMBean,name=classpath");
		if (mbs.isRegistered(classpathName)) {
			mbs.unregisterMBean(classpathName);
		}
	}


	@Test
	public void testSimpleLoadingFromClassPath() {
		final PortletMappingBean pmbUrl = ConfigLoader.getInstance("url");
		Assert.assertNotNull(pmbUrl);
		Assert.assertNotNull(pmbUrl.getActions());
		Assert.assertNotNull(pmbUrl.getReactions());
		Assert.assertNotNull(pmbUrl.getRedirections());
		TestFilters.assertEmpty(pmbUrl.getActions());
		TestFilters.assertEmpty(pmbUrl.getReactions());
		TestFilters.assertEmpty(pmbUrl.getRedirections());

		final PortletMappingBean pmbClassPath = ConfigLoader.getInstance("classpath");
		Assert.assertNotNull(pmbClassPath);
		Assert.assertNotNull(pmbClassPath.getActions());
		Assert.assertNotNull(pmbClassPath.getReactions());
		Assert.assertNotNull(pmbClassPath.getRedirections());
		TestFilters.assertOneEntry(pmbClassPath.getActions());
		TestFilters.assertOneEntry(pmbClassPath.getReactions());
		TestFilters.assertOneEntry(pmbClassPath.getRedirections());

		TestFilters.assertContains(pmbClassPath.getActions(), "classpath");
		TestFilters.assertContains(pmbClassPath.getReactions(), "classpath");
		TestFilters.assertContains(pmbClassPath.getRedirections(), "classpath");
	}


	@Test
	public void testLoadingFromUrl() {
		System.setProperty(ConfigLoader.ENV_WEBLAB_URL, "file:out-of-classpath-mapping.xml");

		final PortletMappingBean pmbClassPath = ConfigLoader.getInstance("classpath");
		Assert.assertNotNull(pmbClassPath);
		Assert.assertNotNull(pmbClassPath.getActions());
		Assert.assertNotNull(pmbClassPath.getReactions());
		Assert.assertNotNull(pmbClassPath.getRedirections());
		TestFilters.assertEmpty(pmbClassPath.getActions());
		TestFilters.assertEmpty(pmbClassPath.getReactions());
		TestFilters.assertEmpty(pmbClassPath.getRedirections());


		final PortletMappingBean pmbUrl = ConfigLoader.getInstance("url");
		Assert.assertNotNull(pmbUrl);
		Assert.assertNotNull(pmbUrl.getActions());
		Assert.assertNotNull(pmbUrl.getReactions());
		Assert.assertNotNull(pmbUrl.getRedirections());
		TestFilters.assertOneEntry(pmbUrl.getActions());
		TestFilters.assertOneEntry(pmbUrl.getReactions());
		TestFilters.assertOneEntry(pmbUrl.getRedirections());

		TestFilters.assertContains(pmbUrl.getActions(), "url");
		TestFilters.assertContains(pmbUrl.getReactions(), "url");
		TestFilters.assertContains(pmbUrl.getRedirections(), "url");
	}


	private static void assertContains(Map<String, ?> map, String key) {
		for (final Entry<String, ?> entry : map.entrySet()) {
			Assert.assertTrue(entry.getKey() + " - " + key, entry.getKey().contains(key));
			if (entry.getValue() instanceof List) {
				for (final Object o : (List<?>) entry.getValue()) {
					Assert.assertTrue(o.toString() + " - " + key, o.toString().contains(key));
				}
			} else if (entry.getValue() instanceof String) {
				Assert.assertTrue(entry.getValue().toString() + " - " + key, entry.getValue().toString().contains(key));
			}
		}

	}


	/**
	 * @param actions
	 */
	private static void assertEmpty(Map<?, ?> map) {
		Assert.assertTrue(map.isEmpty());
	}


	/**
	 * @param actions
	 */
	private static void assertOneEntry(Map<?, ?> map) {
		Assert.assertEquals(1, map.size());
	}

}
